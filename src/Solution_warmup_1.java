import java.util.*;

public class Solution_warmup_1 {

	public int Solution (){
        int sum = 0;
        Scanner in = new Scanner(System.in);
        int length = in.nextInt();
        for (int i=0;i < length;i++) {
            sum += in.nextInt();
        }
        in.close();
        return sum;
    }
    public static void main(String[] args) {
        Solution_warmup_1 sumLines = new Solution_warmup_1();
        System.out.println(sumLines.Solution());
    }
}
