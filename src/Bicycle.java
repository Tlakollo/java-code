/**
 * Bicycle class used for then calling it in the BicycleDemo class to 
 * invoke different types of Bicycle
 */
public class Bicycle {
	int cadence = 0;
	int speed = 0;
	int gear = 1;
	
	void change_cadence (int cadence){
		this.cadence = cadence;
	}
	void speedUp (int speed){
		this.speed = this.speed + speed;
	}
	void speedDown (int speed){
		this.speed = this.speed - speed;
	}
	void brake (){
		this.speed=0;
	}
	void change_gear (int gear){
		this.gear = gear;
	}
	void print_states(){
		System.out.println("Cadence is " + this.cadence + ", number of Gears are " +
							this.gear + " and the speed is " + this.speed);
	}
}
