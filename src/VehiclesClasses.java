class StreetRacer extends VehicleClass
{
    public StreetRacer()
    {
        setGoAlgorithm(new GoByDriving());
    }
}

class FormulaOne extends VehicleClass
{
    public FormulaOne()
    {
        setGoAlgorithm(new GoByDriving());
    }
}

class Helicopter extends VehicleClass
{
    public Helicopter()
    {
        setGoAlgorithm(new GoByFlying());
    }
}

class Jet extends VehicleClass
{
    public Jet()
    {
        setGoAlgorithm(new GoByFlyingFast());
    }
}