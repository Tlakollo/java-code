class JsonObject{
    String name;
    JsonType type;
    Object  value;
    jsonObject next;
}
String JSONtoXML(String input){
    jsonObject current=parse(input);
    int tabs=0;
    Stack<jsonObject> myStack = new Stack<jsonObject>();
    StringBuffer result = new StringBuffer();
    do{
        switch current.type
            case Collection:
                result.append('\t'*tabs);
                if(current.name==""){
                    if(myStack.length!=0){
                        result.append('<'+myStack.top.name+'>\n');
                        tabs++;
                    }
                    myStack.push(current);
                    current=current.value;
                    break;
                }
                else{
                    result.append('<'+current.name+'>\n');
                    tabs++;
                    myStack.push(current);
                    current=current.value;
                    break;
                }
            case Array:
                myStack.push(current);
                current=current.value;
                break;
            case Native:
                result.append('\t'*tabs);
                result.append('<'+current.name+'>'+current.value+'<\\'+current.name+'>\n');
                current=current.next;
                break;
            while(current==null){
                if(myStack.length==0)
                    break;
                current=myStack.pop();
                if(current.type==Array || (myStack.length==0 && current.name=="")){
                    current=current.next;
                    continue;
                }
                tabs--;
                result.append('\t'*tabs);
                if(current.name=="" && current.type==Collection)
                    result.append('<\\'+myStack.top.name+'>\n');
                else{
                    result.append('<\\'+current.name+'>\n');
                current=current.next;
                }
            }
    }while(myStack.length!=0);
    return result;
}