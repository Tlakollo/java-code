/**
 * This is an algorithms problem that tries to find out from an array of integers one of the peaks in the array
 * A peak is considered for this problem as a number that in both sides of it has less or equal value to it.
 * In the case of the first/last number you only need to check or only one value, for example if its the first compare
 * the first number with the next number.
 */