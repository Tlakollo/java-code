class toLowerCase {
    public static String lowerCase(String str) {
        char [] newString = str.toCharArray();
        for(int i=0; i<newString.length;i++){
        	if(newString[i]>64 && newString[i]<91){
        		newString[i]+=32;
        	}
        }
    	return String.valueOf(newString);
    }
    public static void main (String args []){
    	String input ="This Is my String";
    	String result = lowerCase(input);
    	System.out.println(result);
    }
}