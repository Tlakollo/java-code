/**
 * This is a problem from a Microsoft Interview that says the following:
 * You need to write a function that calculates if an object is better than the others given an array of objects. 
 * Things to consider are:
 * Object A could be better than object B and object B could be better than object C
 *    but that doesn't mean that object A is better than object C
 * Objects can be better, worst or neither better/worst than other object
 *  
 */