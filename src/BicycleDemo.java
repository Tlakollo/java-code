/**
 * Used to test the Bicycle function
 * @author Tlakaelel Axayakatl
 *
 */
public class BicycleDemo {
	public static void main (String args[]){
		//Create two different instances of bicycle
		Bicycle bike1 =  new Bicycle();
		Bicycle bike2 =  new Bicycle();
		
		//Invoke methods
		bike1.change_cadence(30);
		bike2.change_cadence(40);
		bike1.speedUp(10);
		bike2.speedUp(50);
		bike1.change_gear(2);
		bike2.change_gear(1);
		bike1.print_states();
		bike2.print_states();
		bike2.speedDown(12);
		bike1.brake();
		bike1.print_states();
		bike2.print_states();
	}
}
