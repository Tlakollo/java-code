/**
 * Basic functionality of Arrays in java along with some binary search functionality
 * @author Tlakaelel Axayakatl
 *
 */
public class Array {
	public int get_min(int[] array){
		int last = array.length-1;
		int begin = 0;
		int center = last/2;
			
	while (!(center==last || center==begin)){
			if (array[center] > array[last]){
				begin = center;
				center = center + (last-center)/2;
			}
			else{
				last = center;
				center = center - (center-begin)/2;
			}
		}
		return array[center];
	}
	
	public static void main(String[] args) {
		int [] array = new int [10];
		array[0] = 4;
		array[1] = 5;
		array[2] = 6;
		array[3] = 7;
		array[4] = 8;
		array[5] = 9;
		array[6] = 10;
		array[7] = 1;
		array[8] = 2;
		array[9] = 3;
		int[] array2 = {14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,0,1,2,3,4,5,6,7,8,9,10,11,12,13};
		int my_value = 50;
		Array my_array = new Array();
		my_value = my_array.get_min(array);
		System.out.println("The minimum value is " + my_value);
		my_value = my_array.get_min(array2);
		System.out.println("The minimum value is " + my_value);
	}

}
