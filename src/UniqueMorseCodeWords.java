import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * International Morse Code defines a standard encoding where each letter is mapped 
 * to a series of dots and dashes, as follows: "a" maps to ".-", "b" maps to "-...", 
 * "c" maps to "-.-.", and so on.
 * 
 * For convenience, the full table for the 26 letters of the English alphabet is given below:
 * [".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",
 * ".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."]
 * 
 * Now, given a list of words, each word can be written as a concatenation of the Morse code of
 * each letter. For example, "cab" can be written as "-.-.-....-", (which is the concatenation 
 * "-.-." + "-..." + ".-"). We'll call such a concatenation, the transformation of a word.
 * 
 * Return the number of different transformations among all words we have.
 * 
 * Example:
 * Input: words = ["gin", "zen", "gig", "msg"]
 * Output: 2
 * 
 * Explanation:
 * The transformation of each word is:
 * "gin" -> "--...-."
 * "zen" -> "--...-."
 * "gig" -> "--...--."
 * "msg" -> "--...--."
 * 
 * There are 2 different transformations, "--...-." and "--...--.".
 * 
 * Note:
 * The length of words will be at most 100.
 * Each words[i] will have length in range [1, 12].
 * words[i] will only consist of lowercase letters.
 */

class UniqueMorseCodeWords {
    public String transformation(String word){
        char iterator;
        char [] phrase = word.toCharArray();
        String [] morse = new String[] {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};
        String transformedWord="";
        for (iterator=0; iterator < phrase.length; iterator++){
            transformedWord += morse[phrase[iterator]-97];        
        }
        return transformedWord;
    }
    @SuppressWarnings("rawtypes")
	public int uniqueMorseRepresentations(String[] words) {
        HashMap <String, Integer> morseRepresentation = new HashMap <String, Integer>();
        char i;
        int uniqueMorseRepresentation=0;
        for (i=0;i<words.length;i++){
            morseRepresentation.put(transformation(words[i]),1);
        }
		Set set = morseRepresentation.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            uniqueMorseRepresentation += (Integer) mentry.getValue();
        }
        return uniqueMorseRepresentation;
    }
}

/*
Leetcode solution
class Solution {
    public int uniqueMorseRepresentations(String[] words) {
        String[] MORSE = new String[]{".-","-...","-.-.","-..",".","..-.","--.",
                         "....","..",".---","-.-",".-..","--","-.",
                         "---",".--.","--.-",".-.","...","-","..-",
                         "...-",".--","-..-","-.--","--.."};

        Set<String> seen = new HashSet();
        for (String word: words) {
            StringBuilder code = new StringBuilder();
            for (char c: word.toCharArray())
                code.append(MORSE[c - 'a']);
            seen.add(code.toString());
        }

        return seen.size();
    }
}

Complexity Analysis
Time Complexity: O(S)O(S), where SS is the sum of the lengths of words in words. We iterate through each character of each word in words.
Space Complexity: O(S)O(S).
*/