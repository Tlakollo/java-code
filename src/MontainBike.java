
public class MontainBike extends Bicycle{
	void climbMontain(){
		this.gear = 2;
		this.speed = 10;
		this.cadence = 20;
	}
	public static void main (String args[]){
		MontainBike bike1 = new MontainBike();
		bike1.change_cadence(10);
		bike1.change_gear(3);
		bike1.speedUp(60);
		bike1.print_states();
		bike1.climbMontain();
		bike1.print_states();
	}
}
