public interface GoAlgorithm
{
    public void go();
}

class GoByDriving implements GoAlgorithm
{
    public void go()
    {
        System.out.println("now I'm Driving.");
    }
}

class GoByFlying implements GoAlgorithm
{
    public void go()
    {
        System.out.println("now I'm Flying.");
    }
}

class GoByFlyingFast implements GoAlgorithm
{
    public void go()
    {
        System.out.println("now I'm Flying really fast!.");
    }
}