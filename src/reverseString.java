class reverseString {
    public static String reverse(String s) {
        char [] newString = s.toCharArray();
        int begin=0, end=newString.length-1;
        char temp;
        while(begin<end){
        	temp=newString[begin];
        	newString[begin]=newString[end];
        	newString[end]=temp;
        	begin++;
        	end--;
        }
    	return String.valueOf(newString);
    }
    public static void main (String args []){
    	String input ="This is my stringa";
    	String result = reverse(input);
    	System.out.println(result);
    }
}