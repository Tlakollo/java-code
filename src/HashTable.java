
public class HashTable {

	int sizeTable = 10;
	item [] MyHashTable = new item[sizeTable];
	
	public HashTable(){
		for (int i = 0; i < MyHashTable.length; i++){
			MyHashTable[i] = new item();
			MyHashTable[i].key = "empty";
			MyHashTable[i].drink = "empty";
			MyHashTable[i].next = null;
		}
	}
	public int hash(String key){
		return Math.abs(key.hashCode()%sizeTable);
	}
	public void addItem (String key, String drink){
		int index = hash(key);
		if (MyHashTable[index].key.equals("empty")){
			MyHashTable[index].key = key;
			MyHashTable[index].drink = drink;
		}
		else {
			item current = MyHashTable [index];
			item newItem = new item();
			newItem.key = key;
			newItem.drink = drink;
			newItem.next = null;
			while (current.next != null){
				current = current.next;
			}
			current.next = newItem;
		}
	}
	public int NumberOfItemsOnIndex (int index){
		int count = 0;
		if(index < sizeTable){
			item current = MyHashTable[index];
			if(current.key.equals("empty")){
				return count;
			}
			else{
				count ++;
				while (current.next != null){
					current = current.next;
					count ++;
				}
				return count;
			}
		}
		return count;
	}
	public void printTable(){
		for (int i = 0; i < MyHashTable.length; i++){
			int number = NumberOfItemsOnIndex(i);
			System.out.println("----------------------");
			System.out.println("On Index "+ i+ " I have:");
			System.out.println(MyHashTable[i].key);
			System.out.println(MyHashTable[i].drink);
			System.out.println("# of items under "+ number);
		}
	}
	public void printItemsInIndex(int index){
		item current = MyHashTable [index];
		System.out.println("-------------------------------------");
		System.out.println("Index "+ index + " has the following values:");
		while (current.next != null){
			System.out.println(current.key);
			System.out.println(current.drink);
			current = current.next;
			System.out.println("------------------------------------");
		}
		System.out.println(current.key);
		System.out.println(current.drink);
		current = current.next;
	}
	public void FindDrink(String key){
		int index = hash(key);
		boolean foundName = false;
		String drink ="";
		item current = MyHashTable[index];
		if(current.key.equals(key)){
			foundName=true;
			drink = current.drink;
		}
		while(current.next != null){
			if(current.key.equals(key)){
				foundName=true;
				drink = current.drink;
			}
			current = current.next;
		}
		System.out.println("----------------------------------------------");
		if(foundName){
			System.out.println("the favorit drink of "+key+" is "+drink);
		}
		else{
			System.out.println("key not found");
		}
	}
	public void ChangeDrink(String key, String drink){
		int index = hash(key);
		boolean foundName = false;
		item current = MyHashTable[index];
		if(current.key.equals(key)){
			foundName=true;
			current.drink = drink;
		}
		while(current.next != null){
			if(current.key.equals(key)){
				foundName=true;
				current.drink = drink;
			}
			current = current.next;
		}
		System.out.println("----------------------------------------------");
		if(foundName){
			System.out.println("the favorit drink of "+key+" has changed to "+drink);
		}
		else{
			System.out.println("key not found");
		}
	}
	
	public static void main(String[] args) {
		HashTable table =  new HashTable();
		table.addItem("Paul", "Coca-cola");
		table.addItem("Tlaka", "Fanta");
		table.addItem("Lizeth", "Coffe");
		table.addItem("Lola", "Kalua");
		table.addItem("Daniel", "juice");
		table.printTable();
		int index = 2;
		table.printItemsInIndex(index);
		table.FindDrink("Tlaka");
		table.ChangeDrink("Tlaka", "Coffe");
	}
}
class item{
	String key;
	String drink;
	item next;
}
