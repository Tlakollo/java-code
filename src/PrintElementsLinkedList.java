
public class PrintElementsLinkedList {

	static Node head;
	public PrintElementsLinkedList(){
		
	}
	public void Print(){
		Node current = head;
		while(current.getNext()!= null){
			System.out.println(current.getData());
			current = current.getNext();
		}
		System.out.println(current.getData());
	}
	public void add(Object data){
		Node temp = new Node(data);
		Node current = head;
		while (current.getNext()!= null){
			current =  current.getNext();
		}
		current.setNext(temp);
	}
	public static void main (String [] args){
		head = new Node(null);
		PrintElementsLinkedList myList = new PrintElementsLinkedList();
		int a = 50;
		int b = 70;
		myList.add(a);
		myList.add(b);
		myList.Print();
	}
}
