
public class LinkedList{
	private Node head;
	private int ListCount;
	public LinkedList(){
		head = new Node(null);
		setListCount(0);
	}
	public void add(Object data){
		Node temp = new Node(data);
		Node current = head;
		while (current.getNext()!= null){
			current =  current.getNext();
		}
		current.setNext(temp);
		setListCount(getListCount() + 1);
	}
	public void add(Object data, int index){
		Node temp = new Node (data);
		Node current = head;
		for (int i = 0;i < index && current.getNext()!= null; i++){
			current = current.getNext();
		}
		temp.setNext(current.getNext());
		current.setNext(temp);
	}
	public Object get(int index){
		Node current = head;
		for (int i = 0;i < index;i++){
			if (current.getNext()== null){
				return null;
			}
			else{
				current = current.getNext();
			}
		}
		return current.getData();
	}
	public int getListCount() {
		return ListCount;
	}
	public void setListCount(int listCount) {
		ListCount = listCount;
	}
	
}
