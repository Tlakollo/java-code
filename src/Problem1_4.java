/*
 * Write a method to replace all spaces in a string with '%20'. You may assume that the
 * string has sufficient space at the end of the string to hold the additional characters,
 * and that you are given the "true" length of the string. (Note: if implementing in Java,
 * please use a character array so that you can perform this operation in place.)
 */

public class Problem1_4 {

	public char [] ReplaceSpaces (char [] my_word, int length){
		int pointer1=length, pointer2=length;
		//Check for the first letter you found from the end of the string 
		while(my_word[pointer1]==' '){
			pointer1--;
		}
		while(pointer1>=0){
			//Replace if space
			if (my_word[pointer1]==' '){
				my_word[pointer2]='0';
				pointer2--;
				my_word[pointer2]='2';
				pointer2--;
				my_word[pointer2]='%';
				pointer2--;
			}
			//Replace if any other letter
			else{
				my_word[pointer2]=my_word[pointer1];
				pointer2--;
			}
			pointer1--;
		}
		return my_word;
	}
	
	public static void main (String [] args){
		String word = "Tlaka & Lizeth    ";
		char [] my_word = word.toCharArray();
		Problem1_4 changechar = new Problem1_4();
		changechar.ReplaceSpaces(my_word, my_word.length-1);
		System.out.println(String.valueOf(my_word));
	}
}
