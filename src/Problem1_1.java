/*
 * Implement an algorithm to determine if a string  has all unique characters. What if you cannot use additional data structures?
 */
public class Problem1_1 {

	public Problem1_1(){
		
	}
	public boolean CheckIfItsUnique (String word){
		if (word.length()>256) return false;
		boolean [] myWord = new boolean [256];
		for (int i=0; i < word.length();i++){
			int Val = word.charAt(i);
			if (myWord[Val]){
				return false;
			}
			myWord[Val]=true;
		}
		return true;
	}
	public static void main (String [] args){
		String word = "Tlakopin";
		Problem1_1 checkString = new Problem1_1();
		boolean isUnique = false;
		isUnique = checkString.CheckIfItsUnique(word);
		System.out.println("The string is unique? "+ isUnique);
	}
}
