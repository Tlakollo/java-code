import java.util.Scanner;


public class RandomChallenge_1 {

	public String calculateSeries (int a, int b, int n){
		String result = "";
		int acumulator = a;
		for (int i = 0; i < n; i++){
			acumulator += (Math.pow(2, i)*b);
			result = result + acumulator + " ";
		}
		return result;
	}
	public static void main (String [] agrs){
		Scanner in = new Scanner (System.in);
		RandomChallenge_1 myChallenge = new RandomChallenge_1();
		int loop = in.nextInt();
		int a, b, n;
		for (int i=0; i < loop;i++){
			a = in.nextInt();
			b = in.nextInt();
			n = in.nextInt();
			System.out.println(myChallenge.calculateSeries(a,b,n));
		}
		in.close();
	}
}
