
public class clock {

	private int hour = 0;
	private int min = 0;
	
	public void set_hour (int hour){
		if (hour <=23 && hour >= 0)
			this.hour = hour %12;
		else
			System.out.println("You put a wrong number");
	}
	public int get_hour (){
		return this.hour;
	}
	public void set_min (int min){
		if (min <=60 && min >= 0)
			this.min = min;
		else
			System.out.println("You put a wrong number");
	}
	public int get_min (){
		return this.min;
	}
	public float get_angle (){
		return (float)(30*this.hour - 5.5*this.min)%360;
	}
	public float get_angle(int hour, int min){
		if (hour <=23 && hour >= 0){
			if (min <=60 && min >= 0)
				return (float)(30*hour - 5.5*min)%360;	
			else{
				System.out.println("You put a wrong number");
				return 0;
			}
		}
		else{
			System.out.println("You put a wrong number");
			return 0;
		}
	}
	public static void main(String[] args) {
		clock clock1 = new clock();
		float angle = 0;
		clock1.set_hour(15);
		clock1.set_min(30);
		angle = clock1.get_angle();
		System.out.println("The angle between the two arrows is: "+ angle);
		angle = clock1.get_angle(3, 0);
		System.out.println("The angle between the two arrows is: "+ angle);
		angle = clock1.get_angle(0, 0);
		System.out.println("The angle between the two arrows is: "+ angle);
		angle = clock1.get_angle(30, 0);
		System.out.println("The angle between the two arrows is: "+ angle);
	}

}
