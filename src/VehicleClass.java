public abstract class VehicleClass
{
    private GoAlgorithm goAlgorithm;
    
    public VehicleClass()
    {
    }
    
    public void setGoAlgorithm(GoAlgorithm algorithm)
    {
        this.goAlgorithm=algorithm;
    }
    
    public void go()
    {
        goAlgorithm.go();
    }
    
}