class HammingDistance {
    public static int measureDistance(int x, int y) {
    	int distance=0;
    	int xor=0;
    	xor=x^y;
    	while(xor>0){ 
          xor &= (xor-1); 
          distance++; 
        } 
    	return distance;
    }
    public static void main (String args []){
    	int x=1;
    	int y=4;
    	int distance = measureDistance(x,y);
    	System.out.println(distance);
    }
}