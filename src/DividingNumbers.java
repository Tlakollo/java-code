/*
self-dividing number is a number that is divisible by every digit it contains.

For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.

Also, a self-dividing number is not allowed to contain the digit zero.

Given a lower and upper number bound, output a list of every possible self dividing number, including the bounds if possible.

Example 1:
Input: 
left = 1, right = 22
Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
Note:

The boundaries of each input argument are 1 <= left <= right <= 10000*/

import java.util.ArrayList;
import java.util.List;

class DividingNumbers {
    public static boolean checkForSelfDividing(int num){
        int originalNum = num;
        while(num!=0){
            if(num%10==0){
                return false;
            }
            else{
                if(originalNum%(num%10)==0){
                    num/=10;
                }
                else{
                    return false;
                }
            }
        }
        return true;
    }
    public static List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> result = new ArrayList<Integer>();
        for (int i=left; i<=right; i++){
            if(checkForSelfDividing(i)){
                result.add(i);
            }
        }
        return result;
    }
    public static void main (String[] args){
    	int left=1;
    	int right=22;
    	List<Integer> result=selfDividingNumbers(left,right);
    }
}