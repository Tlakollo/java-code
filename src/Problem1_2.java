/*
 * Implement a function void reverse (char* str) which reverses a null-terminated string
 */

public class Problem1_2 {
	
	public String reverse (String word){
		char [] copy = word.toCharArray();
		for (int i = 0; i < word.length();i++){
			copy[copy.length-(i+1)] = word.charAt(i);
		}
		return String.valueOf(copy);
	}
	public static void main (String [] args){
		Problem1_2 myWord = new Problem1_2();
		String word = "Tlakollo";
		word = myWord.reverse(word);
		System.out.println(word);
	}
}
