/*
 * Given two strings, write a method to decide if one is a permutation of the other
 */

public class Problem1_3 {
	
	public String sort (String mystring){
		char [] content = mystring.toCharArray();
		java.util.Arrays.sort(content);
		return String.valueOf(content);
	}
	public boolean checkIfPermutation (String x, String y){
		if (x.length() != y.length()){
			return false;
		}
		else {
			return sort(x).equals(sort(y));
		}
	}
	
	public static void main (String [] args){
		Problem1_3 compare = new Problem1_3();
		boolean isPermutation =false;
		isPermutation = compare.checkIfPermutation("Tlakollo", "kolloTla");
		System.out.println("Check Permutation result is: " + isPermutation);
		isPermutation = compare.checkIfPermutation("Tlakollo", "kollotla");
		System.out.println("Check Permutation result is: " + isPermutation);
	}
}
