import java.util.Scanner;


public class FunnyString {

	public String checkIfFunny (String word){
		char [] charWord = word.toCharArray();
		for(int i=1;i<word.length();i++){
			if(Math.abs(charWord[i]-charWord[i-1]) != Math.abs(charWord[word.length()-(i)]-charWord[word.length()-(1+i)])){
				return "Not Funny";
			}
		}
		return "Funny";
	}
	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		FunnyString myString = new FunnyString();
		int loop = in.nextInt();
		in.nextLine();
		for (int i=0; i< loop; i++){
			System.out.println(myString.checkIfFunny(in.nextLine()));
		}
		in.close();
	}
}
